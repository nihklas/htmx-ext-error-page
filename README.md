# HTMX Error Page

This is primarily a debug Tool to work with HTMX.
It is an HTMX-Extension that opens any failed HTMX Requests in a new browser Tab.
This helps debugging your server responses.

## Enabling the extension

```html
<body hx-ext="err-page">
...
</body>
```

## Configuration

You can configure some of the behaviour with the following configurations:

- `enabled`
- `eventName`
- `isError()`
- `errorHandler()`

#### `enabled`

This enables or disables the whole functionality. You can use this in conjunction with an environment variable, to disable the extension in production, and reenable to debug.

default: `true`

#### `eventName`

This is the event, on which the extension listens. When this event fires, the whole error handling takes place.

default: `'htmx:beforeOnLoad'`

#### `isError()`

This is a function, that returns either true or false. It decides if the request should be handled as an error or not. It takes the htmx-event as a parameter.

When you change `eventName`, you may also change this function.

See the htmx documentation for the configured event.

default:
```js
function(evt) {
    return evt.detail.xhr.status >= 400;
}
```

#### `errorHandler()`

This is a function, that handles the request that `isError()` deemed as error. Per default, it opens a new browser tab, with the full html content of the errored response.
It also takes the htmx event as a parameter.

default:
```js
function(evt) {
    let tab = window.open('about:blank', '_blank');
    tab.document.write(evt.detail.xhr.responseText);
    tab.document.close();
}
```



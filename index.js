htmx.config.errPage = {
    eventName: 'htmx:beforeOnLoad',
    enabled: true,
    isError: function(evt) {
        return evt.detail.xhr.status >= 400;
    },
    errorHandler: function(evt) {
        let tab = window.open('about:blank', '_blank');
        tab.document.write(evt.detail.xhr.responseText);
        tab.document.close();
    }
};

const extension = {
    onEvent: function(name, evt) {
        config = htmx.config.errPage;
        if (!config.enabled) {
            return true;
        }
        if (name !== config.eventName) {
            return true;
        }

        if (!config.isError(evt)) {
            return true;
        }

        config.errorHandler(evt);
        return true;
    }
};

htmx.defineExtension('err-page', extension);
